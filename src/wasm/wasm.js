export async function createWasmInstance()
{
  const wasmModule = await WebAssembly.compileStreaming(fetch("code.wasm"));
  const instance = await WebAssembly.instantiate(wasmModule, { env: {}});
  return instance;
}