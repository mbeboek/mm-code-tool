import { useCallback, useEffect, useState } from 'react';
import './App.css';
import { createWasmInstance } from './wasm/wasm';

let wasmInst = undefined;

const MAX_U32 = 0xFFFF_FFFF >>> 0;
const SEARCH_STEPS = 0x00C0_0000 >>> 0;


function sleepMs(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

async function init()
{
  if(!wasmInst) {
    wasmInst = await createWasmInstance();
  }
}

async function checkCodes(target, onProgress)
{
  if(!wasmInst)return;

  console.log("start check");
  console.time("checkCodes");
  const ram = new Uint8Array(wasmInst.exports.memory.buffer);
  
  const results = [];
  for(let i=0; i<MAX_U32; ++i) 
  {
    const rangeStart = i;
    const rangeEnd = Math.min(i + SEARCH_STEPS, MAX_U32) >>> 0;

    const res = wasmInst.exports.mm_codeCheck(rangeStart, rangeEnd, 
        target[0], target[1], target[2],
        target[3], target[4], target[5],
        target[6], target[7], target[8]
    ) >>> 0;
    
    if(res === 0) {
      i = rangeEnd;
    } else {
      i = res;
    }

    if(res !== 0) {
        let ptr = wasmInst.exports.mm_generateCodeStruct(i);
        
        results.push({
          lottery: [
            ram[ptr++], ram[ptr++], ram[ptr++],
            ram[ptr++], ram[ptr++], ram[ptr++],
            ram[ptr++], ram[ptr++], ram[ptr++],
          ],
          mask: [
            ram[ptr++], ram[ptr++], ram[ptr++],
            ram[ptr++], ram[ptr++], ram[ptr++],
          ],
          bomb: [
            ram[ptr++], ram[ptr++], ram[ptr++],
            ram[ptr++], ram[ptr++],
          ]
        });
    }

    await sleepMs(0);
    onProgress(i);
  }
  console.timeEnd("checkCodes");
  return results;
}

function App() 
{
  const [lotteryVals, setLotteryVals] = useState([8,6,1, 3,3,1, 9,2,9]);
  const [bomberVals, setBomberVals] = useState(["-", "-", "-", "-", "-"]);
  const [results, setResults] = useState([]);
  const [progress, setProgress] = useState(-1);
  
  useEffect(() => 
  {
    init().then(() => {
      //setSearchCodes([8,6,1, 3,3,1, 9,2,9]);
    }).catch(e => console.error(e));
  }, []);

  const setLotteryVal = useCallback((idx, ev) => {
    let val = parseInt(ev.target.value) || 0;
    val = Math.min(Math.max(val, 0), 9);

    const newVals = [...lotteryVals];
    newVals[idx] = val;
    setLotteryVals(newVals);
  }, [lotteryVals]);

  const setBomberVal = useCallback((idx, ev) => {
    let valStr = ev.target.value;

    // remove "-" when first changed value
    if(valStr.length > 1 && valStr.startsWith("-")) {
      valStr = valStr.substr(1);
    }

    // Get int value (even if not an int)
    let val = parseInt(valStr) || 0;
    val = Math.min(Math.max(val, 0), 5);

    const newVals = [...bomberVals];
    newVals[idx] = (valStr === (val+"")) ? val : "-"; // clear if not an int
    setBomberVals(newVals);
  }, [bomberVals]);

  const doSearch = useCallback(() => 
  {
    if(lotteryVals.length === 0)return;

    setResults([]);
    checkCodes(lotteryVals, (currentIdx) => {
      const perc = (currentIdx / 0xFFFF_FFFF) * 100;
      setProgress(perc);
    }).then(res => {
      setResults(res);
      setProgress(-1);

      // Sanity check
      for(const entry of res) {
        if(entry.lottery.join("") !== lotteryVals.join("")) {
          console.error("Invalid Result!", res);
        }
      }
    });
  }, [lotteryVals]);
      
  //checkCodes([6,4,7, 1,3,7, 9,3,3]);
  //checkCodes([9,6,4, 3,6,2, 8,9,4]);
  //checkCodes([4,6,0, 3,2,2, 1,4,4]);

  // Filter (Bomber's code)
  
  let filteredResults = results.filter(
    entry => (bomberVals[0] === "-" || bomberVals[0] === entry.bomb[0])
      && (bomberVals[1] === "-" || bomberVals[1] === entry.bomb[1])
      && (bomberVals[2] === "-" || bomberVals[2] === entry.bomb[2])
      && (bomberVals[3] === "-" || bomberVals[3] === entry.bomb[3])
      && (bomberVals[4] === "-" || bomberVals[4] === entry.bomb[4])
  );

  return (
      <div className="main">
        <h3>Lottery</h3>
        <div>
          {lotteryVals.map((val, i) =>  <span key={i}>
                <input type="text" min="0" max="9" 
                  onChange={ev => setLotteryVal(i, ev)} 
                  value={val}
                />
                {
                  (i === 2 || i === 5) && <span className="inputSpacer"></span>
                }
              </span>
          )}
        </div>

        <h3>Bomber's Code <span className='hint'>(optional)</span></h3>
        <div>
          {bomberVals.map((val, i) => 
              <input key={i} type="text" min="0" max="5" 
                onChange={ev => setBomberVal(i, ev)} 
                value={val}
              />
          )}
        </div>

        <button disabled={progress >= 0} className='spaceHori' onClick={doSearch}>Scan</button>

        {progress >= 0 && results.length === 0 &&
          <div className='progress'>
            <div className='progressBar' style={{width: progress+"%"}}></div>
            <span>{progress.toFixed(1)}%</span>
          </div>
        }

        { (results.length > 0) &&
          <h3>Mask Codes</h3>
        }

        {filteredResults.map((res, idx) => <div key={idx} className='resultEntry'>

          <div className='maskResult'>
            {res.mask.map((m, i) => 
              <div key={m+"_"+i} className={"mask"+m}>
                {m}
              </div>
            )}
          </div>

          <div className="bombResult">
            <span>Bomber's Code</span>
            {res.bomb.map((b, i) => <div key={b+"_"+i}>
              {b}
            </div>)}
          </div>
          
        </div>)}

        <div className='spaceHori'></div>

        <div className="footer">
          <a rel="noreferrer" href="https://www.youtube.com/channel/UCfPKx8Hfy_YFns-WSCuZfrA" target="_blank">© HailToDodongo</a>
          <a rel="noreferrer" href="https://gitlab.com/mbeboek/mm-code-tool" target="_blank">GitLab</a>
          <a rel="noreferrer" href="https://kibako.gitlab.io/" target="_blank">Kibako</a>
          <a rel="noreferrer" href="https://kibako.gitlab.io/?actor-params" target="_blank">Actor-Params</a>
        </div>
      </div>
  );
}

export default App;
