/**
* @copyright 2023 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/
#include "main.h"

namespace 
{
  #define RAND_MULTIPLIER 1664525
  #define RAND_INCREMENT 1013904223

  u32 sRandInt = 1;
  u32 sRandFloat;

  struct SaveData {
    s8 lotteryCodes[3][3] = {{0}};
    s8 spiderHouseMaskOrder[6] = {0};
    s8 bomberCode[5] = {};
  };

  constinit SaveData saveData{};

  inline f32 Rand_ZeroOne() {
      sRandInt = (sRandInt * RAND_MULTIPLIER) + RAND_INCREMENT;
      sRandFloat = ((sRandInt >> 9u) | 0x3F800000u);
      return *((f32*)&sRandFloat) - 1.0f;
  }

  s16 Rand_S16Offset(s16 base, s16 range) {
    return (s16)(Rand_ZeroOne() * (f32)range) + base;
  }
}

u32 WASM_EXPORT(mm_generateCodeStruct)(u32 seed)
{
  s32 randBombers;
  s16 sp2A;
  s16 i;
  s32 k;
  s16 randSpiderHouse;

  sRandInt = seed;

  saveData.lotteryCodes[0][0] = (s8)Rand_S16Offset(0, 10);
  saveData.lotteryCodes[0][1] = (s8)Rand_S16Offset(0, 10);
  saveData.lotteryCodes[0][2] = (s8)Rand_S16Offset(0, 10);
  saveData.lotteryCodes[1][0] = (s8)Rand_S16Offset(0, 10);
  saveData.lotteryCodes[1][1] = (s8)Rand_S16Offset(0, 10);
  saveData.lotteryCodes[1][2] = (s8)Rand_S16Offset(0, 10);
  saveData.lotteryCodes[2][0] = (s8)Rand_S16Offset(0, 10);
  saveData.lotteryCodes[2][1] = (s8)Rand_S16Offset(0, 10);
  saveData.lotteryCodes[2][2] = (s8)Rand_S16Offset(0, 10);

  i = 0;
  sp2A = Rand_S16Offset(0, 16) & 3;
  k = 6;
  while (i != k) {
      randSpiderHouse = Rand_S16Offset(0, 16) & 3;
      if (sp2A != randSpiderHouse) {
          saveData.spiderHouseMaskOrder[i] = randSpiderHouse;
          i++;
          sp2A = randSpiderHouse;
      }
  }

  do {
      randBombers = Rand_S16Offset(0, 6);
  } while ((randBombers <= 0) || (randBombers >= 6));

  saveData.bomberCode[0] = randBombers;

  i = 1;
  while (i != 5) {
      k = false;

      do {
          randBombers = Rand_S16Offset(0, 6);
      } while ((randBombers <= 0) || (randBombers >= 6));

      sp2A = 0;
      do {
          if (randBombers == saveData.bomberCode[sp2A]) {
              k = true;
          }
          sp2A++;
      } while (sp2A < i);

      if (k == false) {
          saveData.bomberCode[i] = randBombers;
          i++;
      }
  }

  return (u32)&saveData;
}

u32 WASM_EXPORT(mm_codeCheck)(
    u32 randStart,
    u32 randEnd,
    s16 val0, s16 val1, s16 val2,
    s16 val3, s16 val4, s16 val5,
    s16 val6, s16 val7, s16 val8)
{
  for(u32 i = randStart; i<=randEnd; ++i) {
    sRandInt = i;
    if(Rand_S16Offset(0, 10) == val0 &&
      Rand_S16Offset(0, 10) == val1 &&
      Rand_S16Offset(0, 10) == val2 &&
      Rand_S16Offset(0, 10) == val3 &&
      Rand_S16Offset(0, 10) == val4 &&
      Rand_S16Offset(0, 10) == val5 &&
      Rand_S16Offset(0, 10) == val6 &&
      Rand_S16Offset(0, 10) == val7 &&
      Rand_S16Offset(0, 10) == val8
    ){
      return i;
    }
  }
  return 0;
}

